import { Component, OnInit } from '@angular/core';
import {MatrixNode} from './entities/MatrixNode';
import { PerceptionService } from './services/perception.service';
import { PropioceptionService } from './services/propioception.service';
import { MatrixUtils } from './utils/MatrixUtils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'cognitive-robotina';

  constructor(private perceptionService: PerceptionService, private propioceptionService: PropioceptionService) {}

  matrix:MatrixNode[][] = [];
  lifePcnt:number = 100;
  lifeRed:boolean = this.lifePcnt < 30;
  lifeYellow:boolean = this.lifePcnt <= 70 && this.lifePcnt >= 30;
  lifeGreen:boolean = this.lifePcnt > 70;
  robotInMatrix:boolean = false;
  fullBatteryInMatrix:boolean = false;
  robotDied:boolean = false;
  completedTask1Count = 0;
  completedTask2Count = 0;
  completedTask3Count = 0;
  robotTitle="Robotina. Drag me to start";
  task1Title="Drag me to add a task for sweeping the floor";
  task2Title="Drag me to add a task for washing the dishes";
  task3Title="Drag me to add a task to pick up the trash";
  task1TitleInfo="Sweep the floor";
  task2TitleInfo="Wash the dishes";
  task3TitleInfo="Pick up the trash";
  blockTitle="Drag me to add an obstacle";
  battery1Title="Drag me to add a battery with limited charge: "+PerceptionService.battery1TypePoints+"%";
  battery2Title="Drag me to add a battery with unlimited charge: "+PerceptionService.battery2TypePoints+"%";

  initMatrix() {
    for(let y = 0; y < MatrixUtils.COLS; y++) {
      let row:MatrixNode[] = [];
      for (let x = 0; x < MatrixUtils.ROWS; x++) {
        var node: MatrixNode = {isBattery: false, isBlock: false, isRobotOn: false, isTask: false, position: [y,x]};
        row.push(node);
      }
      this.matrix.push(row);
    }
  }

  printMatrix() {
    for (let elem of this.matrix) {
      for (let itm of elem) {
        console.log("itm", itm)
      }
    }
  }

  async ngOnInit(): Promise<void> {
    this.initMatrix();
    this.perceptionService.eventHandler.globalEvent.subscribe(val => {
      if (val == "start_cycle") {
        if (!this.robotDied) {
          this.perceptionService.startCognitiveCycle(this.matrix);
        } else {
          console.log("Robot died!")
        }
      }else if (val.includes("done_task")) {
        var str = val.substring(val.lastIndexOf("_")+1, val.length);
        var type:number = +str;
        if (type == 1) {
          this.completedTask1Count++;
        } else if (type == 2) {
          this.completedTask2Count++;
        } else if (type == 3) {
          this.completedTask3Count++;
        }
      } else if (val.includes("battery_removed")) {
        this.fullBatteryInMatrix = false;
      }
    });
    this.propioceptionService.health.subscribe(val => {
      this.lifePcnt = val;
      this.lifeGreen =  this.lifePcnt > 70;
      this.lifeYellow = this.lifePcnt <= 70 && this.lifePcnt >= 30;
      this.lifeRed = this.lifePcnt < 30;
      if (this.lifePcnt <= 0) {
        this.robotDied = true;
      }
    });
  }

  dragIcon($event:any, type:string, y:number, x:number) {
    $event.dataTransfer.setData("type", type);
    $event.dataTransfer.setData("y",y);
    $event.dataTransfer.setData("x",x);
  }

  allowIconDrop($event:any) {
    $event.preventDefault();
  }

  async removeIcon($event:any, y:number, x: number) {
    $event.preventDefault();
    if (this.fullBatteryInMatrix) {
      if (this.matrix[y] && this.matrix[y][x] && this.matrix[y][x].isBattery 
        && this.matrix[y][x].batteryType?.type == 2) {
          this.fullBatteryInMatrix=false;
        }
    }
    await this.perceptionService.perceiveRemoveEvent(this.matrix, y,x);
  }

  async iconDrop($event:any, y:number, x: number) {
    $event.preventDefault();
    var type = $event.dataTransfer.getData("type");
    var srcY = $event.dataTransfer.getData("y");
    var srcX = $event.dataTransfer.getData("x");
    if (type == 'battery2') {
      this.fullBatteryInMatrix =true;
    } else if (type == 'robot') {
      if (!this.robotInMatrix) {
        this.propioceptionService.decreaseHealth();
      }
      this.robotInMatrix=true;
    }
    if (!this.robotDied) {
      await this.perceptionService.perceiveDropEvent(this.matrix,y,x,type,srcY, srcX);
    }
  }

}
