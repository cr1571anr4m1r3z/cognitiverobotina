import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppGlobalService {

  constructor() { }

  public globalEvent:Subject<String> = new Subject<String>();
}
