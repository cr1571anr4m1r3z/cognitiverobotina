export interface BatteryType {
    type: number;
    batteryLife: number;
}