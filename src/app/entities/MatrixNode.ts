import { BatteryType } from "./BatteryType";
import { RobotTask } from "./RobotTask";

export interface MatrixNode {
    isBlock:boolean;
    isTask:boolean;
    isBattery:boolean;
    isRobotOn:boolean;
    position:[number,number];
    taskType?:RobotTask;
    batteryType?:BatteryType;
    
}