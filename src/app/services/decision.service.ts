import { Injectable } from '@angular/core';
import { MatrixNode } from '../entities/MatrixNode';
import { MatrixUtils } from '../utils/MatrixUtils';
import { BFSNode } from './helpers/BFSNode';
import { MotorCortexService } from './motor-cortex.service';

@Injectable({
  providedIn: 'root'
})
export class DecisionService {

  constructor(private motorService: MotorCortexService) { }

  public async executeDecisionOnTarget(target: MatrixNode, matrix: MatrixNode[][], y:number, x:number) {
    var minPath = this.calculatePath(y,x,matrix,target.position[0], target.position[1]);
    if (minPath)
      await this.motorService.goToPositionUsingPath(y, x, matrix, minPath);
    else {
      console.log("min path not found 4 target", target)
      await MatrixUtils.delay(500);
      let node: MatrixNode = MatrixUtils.getEmptyNodeAtPosition(y, x);
      node.isRobotOn = true;
      matrix[y][x] = node;
    }
  }

  private constructPath(node: BFSNode) : any{
    if (node) {
      var stack = [];
      while (node.parent !== undefined) {
        stack.push(node.position);
        node = node.parent;
      }  
      var path = [];
      while (stack.length > 0) {
        path.push(stack.pop());
      }
      return path;
    }
    return undefined;
  }

  private zeros(dimensions:any):any {
    var array = [];
    for (var i = 0; i < dimensions[0]; ++i) {
        array.push(dimensions.length == 1 ? 0 : this.zeros(dimensions.slice(1)));
    }
    return array;
  }

  public calculatePath(srcY:number, scrX:number, matrix:MatrixNode[][], y:number, x:number): any {
    var queue = [];
    var node: BFSNode = {position: [srcY, scrX]};
    queue.push(node);
    var visited = this.zeros([matrix.length,matrix[0].length]);
    if (!visited[srcY])
      return;
    visited[srcY][scrX] = 1;
    while(queue.length > 0) {
      var n = queue.shift() || node; // we don't expect to have undefied in shift operation
      let curPosition = n.position;
      let curY = curPosition[0];
      let curX = curPosition[1];
      if (curY == -1 || curX == -1)
        return null;
      if (curY == y && curX == x) {
        return this.constructPath(n);
      }
      for(let direction of MatrixUtils.DIRECTIONS) {
        let dstY =  curY + direction[0];
        let dstX = curX + direction[1];
        if (MatrixUtils.isValid(matrix, dstY, dstX, visited)) {
          let futureNode: BFSNode = {position: [dstY, dstX], parent: n};
          queue.push(futureNode);
          visited[dstY][dstX] = 1;
        }
      }
    }
    return undefined;// in case there is no possible path*/
  }

}
