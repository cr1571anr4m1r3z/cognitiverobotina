export interface BFSNode {
    position:[number, number];
    parent?: BFSNode;
}