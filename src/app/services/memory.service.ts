import { Injectable } from '@angular/core';
import { MatrixNode } from '../entities/MatrixNode';
import { MatrixUtils } from '../utils/MatrixUtils';

@Injectable({
  providedIn: 'root'
})
export class MemoryService {
  constructor() { }

  taskMemoryArray:any = [];
  batteryMemoryArray:any = [];
  public static ROBOT_IN_MATRIX:boolean = false;
  public static BATTERY_IN_MATRIX:boolean = false;
  public static ROBOT_DIED:boolean = false;
  y:number=-1;
  x:number=-1;

  public saveInMemory(node: MatrixNode, matrix: MatrixNode[][], y: any, x: any, srcY: any, srcX: any) {
    let obj: any = {};
    obj['pos'] = [y,x];
    obj['node'] = node;
    if (node.isBattery) {
      this.batteryMemoryArray.push(obj);
    } else if (node.isTask) {
      this.taskMemoryArray.push(obj);
    } else if(node.isRobotOn) {
      var pos = this.getRobotPosition(matrix);
      if (pos[0] != -1 && pos[1] != -1) {
        MatrixUtils.setEmptyPosition(matrix, pos[0], pos[1]);
      }
    }
    // add icon to position
    matrix[y][x] = node;
    // clean src position if aplies
    if (srcX != -1 && srcY != -1) {
      MatrixUtils.setEmptyPosition(matrix, srcY, srcX);
      this.removeFromMemory(srcY, srcX);
    }
  }

  public getBatteryMemory() {
    return this.batteryMemoryArray;
  }

  public getTaskMemory() {
    return this.taskMemoryArray;
  }

  public getMemory() {
    return this.batteryMemoryArray.concat(this.taskMemoryArray);
  }

  public getRobotPosition(matrix: MatrixNode[][]): [number,number] {
    for(let itm of matrix) {
      for (let obj of itm) {
        if(obj.isRobotOn) {
          return obj.position;
        }
      }
    }
    if (MemoryService.ROBOT_IN_MATRIX) {
        return [this.y, this.x];
    }
    return [-1,-1];
  }

  public removeFromMemory(y: number, x: number) {
    for (let i = 0; i < this.batteryMemoryArray.length; i++) {
      let obj = this.batteryMemoryArray[i];
      if (obj.pos[0] == y && obj.pos[1] == x) {
        this.batteryMemoryArray.splice(i,1);
        return;
      }
    }
    for (let i = 0; i < this.taskMemoryArray.length; i++) {
      let obj = this.taskMemoryArray[i];
      if (obj.pos[0] == y && obj.pos[1] == x) {
        this.taskMemoryArray.splice(i,1);
        return;
      }
    }
  }

  public setRobotPosition(y: number, x: number) {
    this.y = y;
    this.x = x;
  }

  public clear(matrix: MatrixNode[][], y: number, x: number) {
    for(let i=0; i < matrix.length; i++) {
      for (let j=0; j < matrix[0].length; j++) {
        let obj =matrix[i][j];
        if (obj.isRobotOn) {
        }
        if(obj.isRobotOn && obj.position[0] != y && obj.position[1] != x) {
          MatrixUtils.setEmptyPosition(matrix, obj.position[0], obj.position[1]);
        }
      }
    }
  }

}
