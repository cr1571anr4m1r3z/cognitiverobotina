import { Injectable } from '@angular/core';
import { MatrixNode } from '../entities/MatrixNode';
import { AppGlobalService } from '../app.global.service';
import { MemoryService } from './memory.service';
import { MatrixUtils } from '../utils/MatrixUtils';
import { PropioceptionService } from './propioception.service';

@Injectable({
  providedIn: 'root'
})
export class MotorCortexService {

  constructor(private eventHandler: AppGlobalService, private memoryService: MemoryService,
     private propioceptionService: PropioceptionService) {}
  private millisecondsToWait: number = 500;

  private sleep(ms?:number) {
    let wait = ms || this.millisecondsToWait;
    return  MatrixUtils.delay(wait);
  }

  private moveRobot(prevNode:MatrixNode, currentNode:MatrixNode, matrix:MatrixNode[][]) {
    prevNode.isRobotOn = false;
    matrix[prevNode.position[0]][prevNode.position[1]] = prevNode;
    currentNode.isRobotOn = true;
    matrix[currentNode.position[0]][currentNode.position[1]] = currentNode;
  }

  public async goToPositionUsingPath(srcY:number, scrX:number, matrix:MatrixNode[][], path: any){
    //console.log("going to position using path", path)
    let prevNode: MatrixNode = matrix[srcY][scrX];
    prevNode.isRobotOn = true;
    matrix[srcY][scrX] = prevNode;
    var stop:boolean = false;
    if (path) {
      var curY, curX;
      for(let i = 0; i<path.length; i++) {
        var position = path[i];
        if (stop) {
          MatrixUtils.setEmptyPosition(matrix, curY, curX);
          this.memoryService.setRobotPosition(curY, curX);
          return;
        } else {
          await this.sleep()
          curY = position[0];
          curX = position[1];
          var currentNode: MatrixNode = matrix[curY][curX];
          //move robot
          this.moveRobot(prevNode, currentNode, matrix);
          if (i == (path.length-1)) {
            await this.performAction(matrix, curY, curX);
          }
          prevNode = currentNode;
          this.eventHandler.globalEvent.subscribe(res => {
            if (res == "cancel_others") {
              stop = true;
            }
          });
        }
      }
    } 
  }

  async performAction(matrix: MatrixNode[][], y: any, x:any) {
    var node = matrix[y][x];
    if (node.isTask) {
      var task = node.taskType;
      this.propioceptionService.consumeBattery(task?.points);
      this.eventHandler.globalEvent.next("done_task_"+task?.type);
    } else if (node.isBattery) {
      var battery = node.batteryType;
      this.propioceptionService.chargeBattery(battery?.batteryLife);
      if (battery?.type == 2) {
        MemoryService.BATTERY_IN_MATRIX = false; 
        this.eventHandler.globalEvent.next("battery_removed");
      }
    } 
    MatrixUtils.setEmptyPosition(matrix, y, x);
    this.memoryService.removeFromMemory(y,x);
    var robot: MatrixNode = MatrixUtils.getEmptyNodeAtPosition(y, x);
    robot.isRobotOn = true;
    matrix[y][x] = robot;
    // loop again
    this.eventHandler.globalEvent.next("start_cycle");
  }

}


