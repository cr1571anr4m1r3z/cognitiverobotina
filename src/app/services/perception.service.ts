import { Injectable } from '@angular/core';
import { AppGlobalService } from '../app.global.service';
import { BatteryType } from '../entities/BatteryType';
import { MatrixNode } from '../entities/MatrixNode';
import { RobotTask } from '../entities/RobotTask';
import { MatrixUtils } from '../utils/MatrixUtils';
import { MemoryService } from './memory.service';
import { PlanningService } from './planning.service';

@Injectable({
  providedIn: 'root'
})
export class PerceptionService {
  constructor(private memoryService: MemoryService, private planningService: PlanningService, public eventHandler: AppGlobalService) { }

  public static battery2TypePoints:number = 100;
  public static battery1TypePoints:number = 30;
  task1TypePoints:number = 5;
  task2TypePoints:number = 10;
  task3TypePoints:number = 15;

  public async perceiveDropEvent(matrix:MatrixNode[][],y: any, x: any, type: any, srcY:any, srcX:any){
    var node: MatrixNode = MatrixUtils.getEmptyNodeAtPosition(y,x);
    if (type == 'battery1'){
      var battery: BatteryType = {type:1, batteryLife: PerceptionService.battery1TypePoints};
      node.batteryType = battery;
      node.isBattery = true;
    } else if (type == 'battery2') {
      var battery: BatteryType = {type:2, batteryLife: PerceptionService.battery2TypePoints};
      node.batteryType = battery;
      node.isBattery = true;
      MemoryService.BATTERY_IN_MATRIX = true;
    } else if (type == 'task1') {
      var task: RobotTask = {type: 1, points: this.task1TypePoints};
      node.isTask = true;
      node.taskType = task;
    } else if (type == 'task2') {
      var task: RobotTask = {type: 2, points: this.task2TypePoints};
      node.isTask = true;
      node.taskType = task;
    } else if (type == 'task3') {
      var task: RobotTask = {type: 3, points: this.task3TypePoints};
      node.isTask = true;
      node.taskType = task;
    } else if (type == 'block') {
      node.isBlock = true;
    } else if (type == 'robot') {
      node.isRobotOn = true;
      MemoryService.ROBOT_IN_MATRIX = true;
    }
    // tigger events
    this.memoryService.saveInMemory(node,matrix, y,x,srcY,srcX);
    this.startCognitiveCycle(matrix);
  }

  async startCognitiveCycle(matrix: MatrixNode[][]) {
    this.eventHandler.globalEvent.next("cancel_others");
    await this.planningService.createPlan(matrix);
  }

  public async perceiveRemoveEvent(matrix: MatrixNode[][], y: number, x: number) {
    MatrixUtils.setEmptyPosition(matrix, y, x);
    this.memoryService.removeFromMemory(y,x);
    this.startCognitiveCycle(matrix);
  }
}
