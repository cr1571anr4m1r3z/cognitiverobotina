import { Injectable } from '@angular/core';
import { MatrixNode } from '../entities/MatrixNode';
import { MatrixUtils } from '../utils/MatrixUtils';
import { DecisionService } from './decision.service';
import { MemoryService } from './memory.service';
import { PropioceptionService } from './propioception.service';

@Injectable({
  providedIn: 'root'
})
export class PlanningService {
  
  constructor(private propioceptionService: PropioceptionService, private memoryService: MemoryService, 
    private decisionService: DecisionService) { }

  public async createPlan(matrix: MatrixNode[][]) {
    if (MemoryService.ROBOT_IN_MATRIX) {
      var memory;
      if (this.propioceptionService.getCurrentHealth() < 30 && this.memoryService.getBatteryMemory().length > 0 && this.reachableBatteries(matrix)) {
        memory = this.memoryService.getBatteryMemory();
      } else {
        var all = this.memoryService.getMemory();
        if (all && all.length > 0)
          memory = all;
      } 
      if (memory)
        await this.planTarget(memory, matrix); 
      else  {
        console.log("nothing to do in scene, emtpy memory");
        let pos = this.memoryService.getRobotPosition(matrix);
        let y = pos[0], x = pos[1];
        await MatrixUtils.delay(500);
        let node: MatrixNode = MatrixUtils.getEmptyNodeAtPosition(y, x);
        node.isRobotOn = true;
        matrix[y][x] = node;
      }
        
    }
  }


  reachableBatteries(matrix: MatrixNode[][]) {
    let robotPosition = this.memoryService.getRobotPosition(matrix);
    let robotY = robotPosition[0], robotX = robotPosition[1];
    for (let battery of this.memoryService.getBatteryMemory()) {
      let pos = battery['pos'];
      let y = pos[0], x = pos[1];
      var dist = Math.abs(x - robotX) + Math.abs(y - robotY); 
      if (!this.canDoIt(matrix, y , x, dist, battery['node'], robotY, robotX)) {
        return false;
      }
    }
    return true;
  }

  public async planTarget(arr: [], matrix:MatrixNode[][]) {
    var min = 9999999;
    var pos = this.memoryService.getRobotPosition(matrix);
    var y1 = pos[0];
    var x1 = pos[1];
    var target;
    for(let itm of arr) {
      var y2 = itm['pos'][0];
      var x2 = itm['pos'][1];
      var dist = Math.abs(x2 - x1) + Math.abs(y2 - y1); 
      if (dist < min && this.canDoIt(matrix, y2, x2, dist, itm['node'], y1, x1)) {
        target = itm['node'];
        min = dist;
      }
    }
    if (target) {
      await this.decisionService.executeDecisionOnTarget(target as unknown as MatrixNode, matrix, y1, x1);
    } else {
        console.log("target not found")
        await MatrixUtils.delay(500);
        let node: MatrixNode = MatrixUtils.getEmptyNodeAtPosition(y1, x1);
        node.isRobotOn = true;
        matrix[y1][x1] = node;      
    } 
  }

  private canDoIt(matrix:MatrixNode[][], y: number, x: number, distance: number, target: MatrixNode, robotY:number, robotX: number) {
    // calculate if is possible to reach dest with current battery
    var timeInSecs = Math.ceil(distance / 2);
    var currentHealth = this.propioceptionService.getCurrentHealth();
    var timing = currentHealth - timeInSecs;
    if (timing <= 0) {
      return false;
    } 
    // check if there is available path
    var path = this.decisionService.calculatePath(robotY, robotX, matrix, target.position[0], target.position[1]);
    if (!path)
      return false;
    var node = matrix[y][x];
    if (node.isTask) {
      // check if is battery still enough in case is a task
      var points = node.taskType?.points || 0;
      if (timing - points <= 1) 
        return false;
    } else if (node.isBattery) {
      // check if is convinient to take battery or save it for later
      var type = node.batteryType;
      if (type?.type == 1) {
        var points = node.batteryType?.batteryLife || 0;
        if (timing + points > 100) 
          return false;
      } else {
        if (currentHealth > 55 && this.memoryService.getBatteryMemory().length > 1)
          return false;
      }
    }
    return true;
  }

}
