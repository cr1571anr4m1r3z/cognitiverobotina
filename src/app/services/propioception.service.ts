import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MatrixUtils } from '../utils/MatrixUtils';

@Injectable({
  providedIn: 'root'
})
export class PropioceptionService {
  constructor() { 
  }

  counter = 100;
  health: Subject<number> = new Subject<number>();

  public getCurrentHealth(): number {
    return this.counter;
  }

  chargeBattery(batteryLife: number | undefined) {
    if (batteryLife) {
      this.counter = (this.counter + batteryLife > 100) ? 100 : this.counter + batteryLife;
    }
  }

  consumeBattery(points: number | undefined) {
    if (points) {
      this.counter = (this.counter - points < 0) ? 0 : this.counter - points;
    }
  }

  public async decreaseHealth() {
    while(this.counter > 0) {
      this.counter--;
      await MatrixUtils.delay(1000);
      this.health.next(this.counter);
    }
  }
}
