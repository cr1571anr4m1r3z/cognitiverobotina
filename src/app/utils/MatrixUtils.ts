import { MatrixNode } from "../entities/MatrixNode";

export class MatrixUtils {
    public static ROWS:number = 30;
    public static COLS:number = 50;

    public static DIRECTIONS = [[-1,0], [0,1], [1,0], [0,-1]];

    public static getEmptyNodeAtPosition(y: number, x: number): MatrixNode {
        return {isBattery: false, isBlock: false, isRobotOn: false, isTask: false, position: [y,x]};
    }

    public static setEmptyPosition(matrix:MatrixNode[][],y: number, x:number) {
        var node: MatrixNode = MatrixUtils.getEmptyNodeAtPosition(y,x);
        if (matrix[y] && matrix[y][x])
          matrix[y][x] = node;
    }

    public static isValid(matrix:MatrixNode[][], y:number, x: number, visited?: any):Boolean {
        if (!(y < matrix.length && y >= 0 && x < matrix[0].length && x >= 0 ))
          return false;
        if (visited && visited[y][x] == 1)
            return false;
        return !matrix[y][x].isBlock
    }
    
    public static delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
      }
}